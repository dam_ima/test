﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	public int municionMax = 0;
	public float dps = 0;
	public int puntos = 0;

	public string nombre;
	public string tipo;
}
