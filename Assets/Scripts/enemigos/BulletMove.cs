﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	public float speed;
	public float damage;

	void FixedUpdate () {

		GetComponent<Rigidbody2D>().velocity = transform.up * speed;
	}

	void OnCollisionEnter2D(Collision2D collider) {
		Debug.Log ("OnCollisionEnter2D" + collider.gameObject.name);
		if(this.gameObject.tag.Equals("BulletEnemy")){
			if(collider.gameObject.tag != "Enemy" && collider.gameObject.tag != "Boss")
				Destroy (this.gameObject);
		} else if(this.gameObject.tag.Equals("BulletAlly")) {
			if (collider.gameObject.tag != "Player")
				Destroy (this.gameObject);
		}
	}
}
