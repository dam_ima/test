﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using IMAEnums;

public class EnemyController : MonoBehaviour {

	private const float FINAL_POS = 1;
	private const int FOTOGRAMS_FROM_MOVE = 30;
	private const float FOTOGRAMS_MOVE_INCREMENT = FINAL_POS / FOTOGRAMS_FROM_MOVE;
	private const float HITRATE = 1.5f;//intervalo para inflingir daño al jugador
	public const int MELEE_VISION_RANGE = 4;
	public const int RANGER_VISION_RANGE = 6;
	public const int MELEE_ATTACK_RANGE = 4;
	public const int RANGER_ATTACK_RANGE = 4;
	private const float MAXPERSECUTION = 15;//tiempo maximo de persecucion si no ve al objetivo
	private const float MAX_IDLE_TIME = 2;
	private const int MAX_PATROL_RANGE = 5;
	private const int MIN_PATROL_RANGE = 2;
	public Estado state;//estado actual
	private static List<Vector2> enemysNextMove = new List<Vector2>();
	private int health = 3;
	private GameObject player;
	private RaycastHit hit;//rayo para comprobar si hay vision directa sobre el jugador
	private float timeHit = HITRATE;//tiempo desde la ultima vez que se golpeo al jugador
	private bool isRangoVision;//comprueba si el jugador entra en el trigger que indica que esta en su rango de vision
	private bool isMove = false;
	private bool isInTransitMove = false;
    private Animator anim;
	private float idleTime = MAX_IDLE_TIME;
	private int patrolRange = -1;
	private int patrolMove = 0;
	private int patronDirection;
	private Vector2 finalPos = new Vector2(0,0);
	private float persecutionTime;//tiempo persiguiendo al jugador desde que ha perdido su vision
	public Enemy enemy;
	private static GameManager2 gameManager;
	private static BoardManager2 boardManager;
	//private int[] enemyRotations;
	private int enemyMaxRotPatron = 5;
	private Vector2 positionMove = new Vector2(0,0);
	private GameObject boardPosition = null;
	private bool firstEnemyEncounter = false;
	public bool hasVision = false;
	private Transform[] childrenTransforms;
	public Transform bulletSpawn;
	public GameObject bullet;
	private int damage = 1;
	private AudioSource sonidoDisparo;
	// Use this for initialization
	void Awake() {
        anim = GetComponent<Animator>();
		initializeEnemy ();
		player = GameObject.FindGameObjectWithTag ("Player");
		if(!gameManager) {
			GameObject objGameManager = GameObject.FindGameObjectWithTag ("GameController");
			gameManager = objGameManager.GetComponent<GameManager2> ();
			boardManager= gameManager.GetComponent<BoardManager2> ();
		}
		getChildren ();
		damage += gameManager.level;
		health += gameManager.level;
		sonidoDisparo = gameObject.GetComponent<AudioSource> ();
	}

	void Start () {
		
        idle ();
	}

	void getChildren(){
		childrenTransforms = new Transform[this.transform.childCount];
		Debug.Log ("hay hijos:" + this.transform.childCount);
		for (int i = 0; i < this.transform.childCount; i++) {
			childrenTransforms [i] = this.transform.GetChild (i);
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (state == Estado.idle) {
            idle();
            anim.SetBool("walking",false);
        }
		else if (state == Estado.voltear) {
            voltear();
            anim.SetBool("walking", false);
        }
		else if (state == Estado.patrullar) {
            patrullar();
            anim.SetBool("walking", true);
        }
		else if (state == Estado.perseguir) {
            perseguir();
            anim.SetBool("walking", true);
        }
		else if (state == Estado.atacar) {
            atacar();
        }
		//Debug.Log ("Update: estado actual:" + state);
	}

	private void initializeEnemy(){
		String[] enemyOptions = System.Enum.GetNames (typeof(EnemyTypes));
		int enemySelected = UnityEngine.Random.Range (0, enemyOptions.Length);
		Debug.Log ("Enemigo seleccionado: " + enemyOptions[enemySelected]);

		if(enemyOptions[enemySelected].Equals("PeonEnemy")){
			enemy = new MeleePeonEnemy (damage,health,MELEE_VISION_RANGE,MELEE_ATTACK_RANGE);
		}else if(enemyOptions[enemySelected].Equals("ArcherEnemy")) {
			enemy = new ArcherEnemy (damage,health,RANGER_VISION_RANGE,RANGER_ATTACK_RANGE);
		}
	}

	private int rotatePatron(){
		
		return UnityEngine.Random.Range (0,4);
	}

	private void idle(){
		state = Estado.idle;
		if (idleTime < MAX_IDLE_TIME) {//si no tiene rango de vision incrementar tiempo de persecucion
			idleTime += Time.deltaTime;
		} else {
			idleTime = 0;
			Debug.Log (idleTime);
			if (enemy.isPatrol) {
				patrullar ();
			} else {
				voltear ();
			}
		}
	}

	private void cambiarDireccion(int rotacion){
		Vector3 rotationQuaternion = new Vector3 (0,0,0);

		if (rotacion == 0) {
			rotationQuaternion = new Vector3 (0, 0, 0);
		} else if (rotacion == 1) {
			rotationQuaternion = new Vector3 (0, 0, 90);
		} else if (rotacion == 2) {
			rotationQuaternion = new Vector3 (0, 0, 180);
		} else if (rotacion == 3) {
			rotationQuaternion = new Vector3 (0, 0, 270);
		} else if (rotacion == 4) {
			rotationQuaternion = new Vector3 (0, 0, 45);
		} else if (rotacion == 5) {
			rotationQuaternion = new Vector3 (0, 0, 135);
		} else if (rotacion == 6) {
			rotationQuaternion = new Vector3 (0, 0, 225);
		} else if (rotacion == 7) {
			rotationQuaternion = new Vector3 (0, 0, 315);
		}

        anim.SetFloat("rotacion",rotationQuaternion.z);
        Debug.Log("Direccion en la que esta minado: ------------------------------------->>>> " + rotacion);
		for (int i = 0; i < this.transform.childCount; i++) {
			childrenTransforms [i].rotation = Quaternion.Euler (rotationQuaternion);
		}
	}

	private void voltear(){
		int rotacion = rotatePatron ();
		state = Estado.voltear;
		cambiarDireccion (rotacion);
		idle ();
	}

	private int patrolDirection(){
		return UnityEngine.Random.Range (0,4);
	}

	private void patrullar(){

		if(state != Estado.atacar) {
			state = Estado.patrullar;
			if (patrolRange == -1) {
				patrolRange = UnityEngine.Random.Range (MIN_PATROL_RANGE,MAX_PATROL_RANGE);
				patronDirection = patrolDirection ();
			}
			if (isInTransitMove) {
				fluidMove ();
				//Debug.Log ("Patrullar: esta en transito" + isInTransitMove);
			} else {
				bool isMove = movePatrol ();
				//Debug.Log ("Patrullar: se mueve vale" + isMove);
				if (!isMove) {
					idle ();
					patrolRange = -1;
				} else {
					if (patrolMove < patrolRange) {
						patrolMove++;
						fluidMove ();
					} else {
						patrolMove = 0;
						patrolRange = -1;
						idle ();
					}
				}	
			}
		}
	}

	//asigna la direccion a moverse comprobando si en el tablero del juego existe algun obstaculo
	private void asignDirectionToMove(){
		if ((int)positionMove.y >= 0 && (int)positionMove.y < boardManager.gridObject.Length && (int)positionMove.x >= 0 
			&& (int)positionMove.x < boardManager.gridObject[(int)positionMove.y].Length) {
			if (patronDirection == 0) {
				positionMove = new Vector2 (this.transform.position.x, this.transform.position.y + 1);
				boardPosition = boardManager.gridObject [(int)positionMove.y] [(int)positionMove.x];

			} else if (patronDirection == 1) {
				positionMove = new Vector2 (this.transform.position.x + 1, this.transform.position.y);
				boardPosition = boardManager.gridObject [(int)positionMove.y] [(int)positionMove.x];

			} else if (patronDirection == 2) {
				positionMove = new Vector2 (this.transform.position.x, this.transform.position.y - 1);
				boardPosition = boardManager.gridObject [(int)positionMove.y] [(int)positionMove.x];

			} else if (patronDirection == 3) {
				positionMove = new Vector2 (this.transform.position.x - 1, this.transform.position.y);
				boardPosition = boardManager.gridObject [(int)positionMove.y] [(int)positionMove.x];
			}
            // TODO revisar bien esto

            Debug.Log("Se mueve -----------------------------" + (positionMove != Vector2.zero));
            //anim.SetBool("walking", true);
        } else{

            Debug.Log("No se mueve -----------------------------" + (positionMove != Vector2.zero));
            //anim.SetBool("walking", false);

        }

        //anim.SetFloat("movX", positionMove.x);
        //anim.SetFloat("movY", positionMove.y);

    }

	private void checkPlayerPosition(){
		if (player) {
			Debug.Log ("checkPlayerPosition: posicion Jugador vale:"+ player.transform.position +  
				" posicion enemigo vale:" + this.transform.position);
			float playerX = player.transform.position.x;
			float playerY = player.transform.position.y;
			float enemyX = this.transform.position.x;
			float enemyY = this.transform.position.y;
			if ((int)playerX > (int)enemyX && (int)playerY > (int)enemyY) {
				Debug.Log ("checkPlayerPosition: jugador esta arriba y a la derecha");
				positionMove = new Vector2 (enemyX+1, enemyY + 1);
				cambiarDireccion (7);
			} else if ((int)playerX < (int)enemyX && (int)playerY < (int)enemyY) {
				Debug.Log ("checkPlayerPosition: jugador esta abajo y a la izquierda");
					positionMove = new Vector2 (enemyX-1, enemyY - 1);
				cambiarDireccion (5);
			} else if ((int)playerX > (int)enemyX && (int)playerY < (int)enemyY) {
				Debug.Log ("checkPlayerPosition: jugador esta abajo y a la derecha");
				positionMove = new Vector2 (enemyX +1, enemyY - 1);
				cambiarDireccion (6);
			} else if ((int)playerX < (int)enemyX && (int)playerY > (int)enemyY) {
				Debug.Log ("checkPlayerPosition: jugador esta arriba y a la izquierda");
				positionMove = new Vector2 (enemyX -1, enemyY + 1);
				cambiarDireccion (4);
			} else if ((int)playerX == (int)enemyX && (int)playerY > (int)enemyY) {
				positionMove = new Vector2 (enemyX, enemyY + 1);
				cambiarDireccion (0);
				Debug.Log ("checkPlayerPosition: jugador esta arriba");
			} else if ((int)playerX > (int)enemyX && (int)playerY == (int)enemyY) {
				positionMove = new Vector2 (enemyX +1 , enemyY);
				cambiarDireccion (3);
				Debug.Log ("checkPlayerPosition: jugador esta a la derecha");
			} else if ((int)playerX == (int)enemyX && (int)playerY < (int)enemyY) {
				positionMove = new Vector2 (enemyX, enemyY + -1);
				cambiarDireccion (2);
				Debug.Log ("checkPlayerPosition: jugador esta debajo");
			} else if ((int)playerX < (int)enemyX && (int)playerY == (int)enemyY) {
				positionMove = new Vector2 (enemyX -1, enemyY);
				cambiarDireccion (1);
				Debug.Log ("checkPlayerPosition: jugador esta a la izquierda");
			} 
		} else {
			player = GameObject.FindGameObjectWithTag ("Player");
		}
	}

	private bool movePatrol(){
		bool isObstacle = false;
		bool canMove = true;

		if (boardManager) {
			asignDirectionToMove ();
			//Los enemigos no se almacenan en el board, cambiar para que el enemigo almacene sus coordenadas en un array y comprobar ahi
			if (boardPosition.tag.Equals ("RoomWall") || boardPosition.tag.Equals ("Enemy")) {
				isObstacle = true;
			} else {
				if (enemysNextMove.Count > 0) {
					foreach (Vector2 position in enemysNextMove) {
						if (position.Equals (positionMove)) {
							canMove = false;
						}
					}
				}
			}
			if (canMove) {
				enemysNextMove.Add (positionMove);
				cambiarDireccion (patronDirection);
				finalPos = positionMove;
			} else {
				isObstacle = true;
			}
		}
		return isObstacle;
	}

	private void fluidMove(){
		Vector2 positionMove = new Vector2(0,0);
		bool isReachGoal = false;
		float marginObjetive = 0.05f;
		int floorPosMoveX = (int)positionMove.x;
		int floorPosMoveY = (int)positionMove.y;
		int floorThisPosX = (int)this.transform.position.x;
		int floorThisMoveY = (int)this.transform.position.y;

		if (!isInTransitMove) {
			isInTransitMove = true;
		}
			
		//arriba a la derecha
		if (finalPos.x > this.transform.position.x && finalPos.y > this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x + (FOTOGRAMS_MOVE_INCREMENT / 2), this.transform.position.y +
				(FOTOGRAMS_MOVE_INCREMENT / 2));
			Debug.Log ("fluidMove: posicionMove vale:" + positionMove.x + " finalPos vale:" + finalPos);
			Debug.Log ("fluidMove: posicionFinal x es mayor que actual");
			if ((positionMove.x >= finalPos.x - marginObjetive && positionMove.x <= finalPos.x + marginObjetive) && 
				(positionMove.y >= finalPos.y - marginObjetive && positionMove.y <= finalPos.y + marginObjetive)) {
				isReachGoal = true;
				//Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.x + marginObjetive) + (finalPos.x - marginObjetive));
			}
			//arriba a la izquierda
		} else if (finalPos.x < this.transform.position.x && finalPos.y > this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x - (FOTOGRAMS_MOVE_INCREMENT / 2), this.transform.position.y + 
				(FOTOGRAMS_MOVE_INCREMENT / 2));
			Debug.Log ("fluidMove: posicionMove vale:" + positionMove.x + " finalPos vale:" + finalPos);
			Debug.Log ("fluidMove: posicionFinal y es mayor que actual");
			if ((positionMove.x <= finalPos.x + marginObjetive && positionMove.x >= finalPos.x - marginObjetive) && 
				(positionMove.y >= finalPos.y - marginObjetive && positionMove.y <= finalPos.y + marginObjetive)) {
				isReachGoal = true;
				//Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
			//abajo a la derecha
		} else if (finalPos.x > this.transform.position.x && finalPos.y < this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x + (FOTOGRAMS_MOVE_INCREMENT / 2), this.transform.position.y - 
				(FOTOGRAMS_MOVE_INCREMENT / 2));
			Debug.Log ("fluidMove: posicionMove vale:" + positionMove.x + " finalPos vale:" + finalPos);
			Debug.Log ("fluidMove: posicionFinal x es menor que actual");
			if ((positionMove.x >= finalPos.x - marginObjetive && positionMove.x <= finalPos.x + marginObjetive) && 
				(positionMove.y <= finalPos.y + marginObjetive && positionMove.y >= finalPos.y - marginObjetive)) {
				isReachGoal = true;
				//Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.x + marginObjetive) + (finalPos.x - marginObjetive));
			}
			//abajo a la izquierda
		} else if (finalPos.x < this.transform.position.x && finalPos.y < this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x - (FOTOGRAMS_MOVE_INCREMENT / 2), this.transform.position.y - 
				(FOTOGRAMS_MOVE_INCREMENT / 2));
			Debug.Log ("fluidMove: posicionMove vale:" + positionMove.x + " finalPos vale:" + finalPos);
			Debug.Log ("fluidMove: posicionFinal y es menor que actual");
			if ((positionMove.x <= finalPos.x + marginObjetive && positionMove.x >= finalPos.x - marginObjetive) && 
				(positionMove.y <= finalPos.y + marginObjetive && positionMove.y >= finalPos.y - marginObjetive)) {
				isReachGoal = true;
				//Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
			//solo hacia arriba
		} else if (finalPos.x == this.transform.position.x && finalPos.y > this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x, this.transform.position.y + FOTOGRAMS_MOVE_INCREMENT);
			if (positionMove.y >= finalPos.y - marginObjetive && positionMove.y <= finalPos.y + marginObjetive) {
				isReachGoal = true;
				Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
			//solo hacia la derecha
		} else if (finalPos.x > this.transform.position.x && finalPos.y == this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x + FOTOGRAMS_MOVE_INCREMENT, this.transform.position.y);
			if (positionMove.x >= finalPos.x - marginObjetive && positionMove.x <= finalPos.x + marginObjetive) {
				isReachGoal = true;
				Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
			//solo hacia abajo
		} else if (finalPos.x == this.transform.position.x && finalPos.y < this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x, this.transform.position.y - FOTOGRAMS_MOVE_INCREMENT);
			if (positionMove.y <= finalPos.y + marginObjetive && positionMove.y >= finalPos.y - marginObjetive) {
				isReachGoal = true;
				Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
			//solo hacia la izquierda
		} else if (finalPos.x < this.transform.position.x && finalPos.y == this.transform.position.y) {
			positionMove = new Vector2 (this.transform.position.x - FOTOGRAMS_MOVE_INCREMENT, this.transform.position.y);
			if (positionMove.x <= finalPos.x + marginObjetive && positionMove.x >= finalPos.x - marginObjetive) {
				isReachGoal = true;
				Debug.Log ("fluidMove: objetivo cumplido valores comprendidos entre:" + (finalPos.y + marginObjetive) + (finalPos.y - marginObjetive));
			}
		} else {
			isReachGoal = true;
			isInTransitMove = false;

            //Debug.Log("se esta moviendo: " + (positionMove != Vector2.zero));
		}

		if (isInTransitMove) {
			//Debug.Log ("fluidMove: Esta en transito" + isInTransitMove);
			//Debug.Log ("fluidMove: posicion final vale:" + finalPos);
			//Debug.Log ("fluidMove: posicion actual vale:" + this.transform.position);
			this.transform.position = new Vector3 (positionMove.x,positionMove.y,0);
            anim.SetBool("walking", true);
            /*anim.SetFloat("movX", positionMove.x);
            anim.SetFloat("movY", positionMove.y);*/
        }

		if(isReachGoal){
			isInTransitMove = false;
            anim.SetBool("walking", false);
            /*anim.SetFloat("movX", positionMove.x);
            anim.SetFloat("movY", positionMove.y);*/
            //Debug.Log ("fluidMove: Ha llegado a su destino" + isInTransitMove);
            //enemysNextMove.Remove (finalPos); esto se utiliza tambien por el jugador implementar mas adelante
        }
	}

	private void atacar() {
		Debug.Log ("Atacando jugador");
		if (player) {
			Vector3 dir = player.transform.position - this.transform.position;
			float angle = (Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg)  -90;
			for (int i = 0; i < this.transform.childCount; i++) {
				childrenTransforms [i].rotation = Quaternion.AngleAxis(angle, Vector3.forward);
					//Quaternion.Euler (rotationQuaternion);
			}
			state = Estado.atacar;
			if (timeHit > HITRATE) {
				timeHit = 0;
				if(bulletSpawn) {
					Debug.Log(Vector2.SignedAngle (this.transform.position,player.transform.position));
					//bulletSpawn.LookAt (new Vector3(0,0,player.transform.position.z));
					GameObject instance = Instantiate (bullet, new Vector2 (bulletSpawn.position.x, bulletSpawn.position.y),
						bulletSpawn.rotation);
					sonidoDisparo.Play ();
				}
			} else {
				timeHit += Time.deltaTime;
			}
		}
	}

	private void perseguir(){
		state = Estado.perseguir;
		if (hasVision) {
			Debug.Log ("perseguir: tiene vision");
			persecutionTime = 0;
			if (!firstEnemyEncounter && isInTransitMove) {
				Debug.Log ("perseguir: primer encuentro");
				firstEnemyEncounter = true;
				isInTransitMove = false;
			}
			//if (!isInTransitMove) {
				checkPlayerPosition ();
				finalPos = positionMove;
			//}
			fluidMove ();
		} else {
			Debug.Log ("perseguir: no tiene vision " + persecutionTime);
			persecutionTime += Time.deltaTime;
			if (persecutionTime < MAXPERSECUTION) {
				if (!isInTransitMove) {
					Debug.Log ("perseguir: no se esta moviendo " + isInTransitMove);
					checkPlayerPosition ();
					finalPos = positionMove;
				}
				fluidMove ();
			} else {
				Debug.Log ("perseguir: no tiene vision vuelve a idle");
				persecutionTime = 0;
				idle ();
				firstEnemyEncounter = false;
			}
		}
	}

	private void recibirDano(int damage){
		health -= damage;
		if (health <= 0) {
			muerto ();
		}
	}

	private void muerto(){
		state = Estado.muerto;
		if(this.gameObject.tag.Equals("Boss")) {
			gameManager.SendMessage("nextLevel");
		}
		Destroy (gameObject);
	}

	void OnCollisionEnter2D(Collision2D collider) {
		if (collider.gameObject.tag.Equals ("BulletAlly")) {
			recibirDano (1);
			Destroy (collider.gameObject);
			if(state != Estado.perseguir && state != Estado.atacar){
				state = Estado.perseguir;
			}
		}
	}
}