﻿using UnityEngine;
using System.Collections;
using IMAEnums;

public class Enemy : AbstractEnemy
{
	public Enemy(int damage, int health,int visionRange,int attackRange){
		this.damage = damage;
		this.health = health;
		//this.enemyElem = elem;
		this.attackRange = attackRange;
		this.visionRange = visionRange;
		int patrolOption = Random.Range (0,2);

		if (patrolOption == 0) {
			this.isPatrol = false;
		} else if(patrolOption == 1) {
			this.isPatrol = true;
		}

	}

	protected override void recibeDamage (int damage, Elemento enemyElem){

	}

	protected override void sendDamage (int sendDamage, Elemento enemyElem){

	}
}

