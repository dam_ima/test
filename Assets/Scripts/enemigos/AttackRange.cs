﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour {

	EnemyController enemyController;
	BoxCollider2D boxCollider;
	// Use this for initialization
	void Start () {
		enemyController = gameObject.GetComponentInParent (typeof(EnemyController)) as EnemyController;
		boxCollider = gameObject.GetComponent<BoxCollider2D> ();

		if (enemyController.enemy.attackRange == EnemyController.MELEE_ATTACK_RANGE) {
			boxCollider.size = new Vector2 (boxCollider.size.x + EnemyController.MELEE_ATTACK_RANGE, boxCollider.size.y + EnemyController.MELEE_ATTACK_RANGE);
		} else {
			boxCollider.size = new Vector2 (boxCollider.size.x + EnemyController.RANGER_VISION_RANGE, boxCollider.size.y + EnemyController.RANGER_VISION_RANGE);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log ("ENTRA TRIGGER");
		if (collider.gameObject.CompareTag("Player")) {
			enemyController.SendMessage ("atacar");
		}
	}

	void OnTriggerStay2D(Collider2D collider){
		if (collider.gameObject.CompareTag("Player")) {
			enemyController.SendMessage ("atacar");
		}
	}

	void OnTriggerExit2D(Collider2D collider){
		if (collider.gameObject.CompareTag("Player")) {
			enemyController.SendMessage ("perseguir",true);
		}
	}

}
