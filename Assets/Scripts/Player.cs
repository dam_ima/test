﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//Variables
	public static Player instance = null;
	private Animator anim;
	private float tiempoDesdeRecarga = 0;
	private float tiempoUltimoDisparo = 0;
	public float tiempoEntreDisparos = 1;
	public float velocidad = 3;
	public float puntosDeVida = 3;
	public float puntosDeVidaMax = 3;
	public float puntosDeVidaMaxTope = 8;
	public float puntosDeDaño = 1;
	public float armadura = 0.1F;
	public float tiempoRecarga = 2;
	private GameManager2 gameManager;
	public int municionActual = 10;
	public int municionTotal = 10;
	protected Joystick joystick;
	private bool esRecarga = false;
	public GameObject armaInicial;
	//Referencias a las vidas que le quedan al jugador y que se presentan en pantalla
	public Image[] ImagenesVidas = new Image[8];
	public Image[] ImagenesVidasVacias = new Image[8];
	public Image[] ImagenesBalas = new Image[31];
	public GameObject[] armas;
	public GameObject gofre;
	public GameObject bebida;
	public GameObject bala;
	public GameObject apuntador;
	public GameObject mira;
	public GameObject cañon;
	public Slider recarga;
	private Vector3 movimientoRaton;

	private AudioSource sonidoDisparo;

	//Se ejecuta al principio
	void Awake (){
		
		if(instance == null){
			instance = this;
		}
		else if (instance != this){
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);

		for(int i = 0; i <= 7; i++){

			ImagenesVidas [i] = GameObject.Find ("Vida" + (i+1)).GetComponent<Image>();

		}

		for(int i = 0; i<=7; i++){

			ImagenesVidasVacias [i] = GameObject.Find ("VidaVacia" + (i+1)).GetComponent<Image>();

		}

		for(int i = 0; i<31; i++){

			ImagenesBalas [i] = GameObject.Find ("ib" + i).GetComponent<Image>();

		}

		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor) {
			GameObject mobileButton = GameObject.Find ("Fixed Joystick");
			joystick = FindObjectOfType<Joystick> ();
		} else {
			GameObject mobileButton = GameObject.Find ("Fixed Joystick");
			mobileButton.SetActive (false);
		}
		recarga = GameObject.Find ("Recarga").GetComponent<Slider>();
		recarga.gameObject.SetActive (false);
		actualizarInterfaz ();
		gameManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager2>();
		Debug.Log("Awake ejecutado");
		equiparArma (armaInicial);
	}

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		recarga.minValue = 0;
		recarga.maxValue = tiempoRecarga;
		sonidoDisparo = gameObject.GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		
		tiempoUltimoDisparo += Time.deltaTime;
		tiempoDesdeRecarga += Time.deltaTime;

		if (esRecarga && tiempoDesdeRecarga <= tiempoRecarga) {

			recarga.value = (tiempoDesdeRecarga * tiempoRecarga);
		}if (recarga.value == recarga.maxValue && esRecarga) {
			esRecarga = false;
			recarga.value = 0;
			recarga.gameObject.SetActive (false);
			municionActual = municionTotal;
			actualizarInterfaz ();
		}

		tiempoUltimoDisparo += Time.deltaTime;
		tiempoDesdeRecarga += Time.deltaTime;

		if (esRecarga && tiempoDesdeRecarga <= tiempoRecarga) {

			recarga.value = (tiempoDesdeRecarga * tiempoRecarga);
		}if (recarga.value == recarga.maxValue && esRecarga) {
			esRecarga = false;
			recarga.value = 0;
			recarga.gameObject.SetActive (false);
			municionActual = municionTotal;
			actualizarInterfaz ();
		}

		if (Application.platform == RuntimePlatform.Android) {
			palancaIzquierda (joystick.Horizontal, joystick.Vertical);
			palancaDerecha (joystick.Horizontal, joystick.Vertical * -1);

			if (Input.touchCount > 1) {
				fire1 ();
			}
			if (municionActual == 0) {
				fire2 ();
			}
		} else {
			//Si se utilizan palancas
			if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) {
				palancaIzquierda (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
			}else {
				anim.SetBool("walking", false);
			}
			if (Input.GetAxis ("HorizontalD") != 0 || Input.GetAxis ("VerticalD") != 0) {

				float x = Input.GetAxis ("HorizontalD");
				float y = Input.GetAxis ("VerticalD");
				palancaDerecha (Input.GetAxis ("HorizontalD"), Input.GetAxis ("VerticalD"));

			} else if (mira.transform.position != Input.mousePosition){

				// Transformar las cordenadas del raton en cordenads del mundo
				Vector2 ratonEnElMundo = Camera.main.ScreenToWorldPoint(Input.mousePosition);

				float AngleRad = Mathf.Atan2(ratonEnElMundo.y - apuntador.transform.position.y, ratonEnElMundo.x - apuntador.transform.position.x);
				float AngleDeg = (180 / Mathf.PI) * AngleRad;
				apuntador.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);

				/*movimientoRaton = new Vector3 (Input.mousePosition.x - mira.transform.position.x, Input.mousePosition.y - mira.transform.position.y, 0);
			mira.transform.position = Input.mousePosition;
			apuntador.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, ((Mathf.Atan2 (-1 * movimientoRaton.y, movimientoRaton.x)) * Mathf.Rad2Deg)));*/
			}
			//Si se pulsan botones
			if (Input.GetAxis ("Fire1") != 0) {
				fire1 ();
				Debug.Log ("1");
			}
			if (Input.GetAxis ("Fire2") != 0) {
				fire2 ();
				Debug.Log ("2");
			}
			if (Input.GetAxis ("Fire3") != 0) {
				Debug.Log ("3");
				fire3 ();
			}
			if (Input.GetAxis ("Jump") != 0) {
				Debug.Log ("4");
				fire4 ();
			}
		}
	}

	//Este metodo es llamado al utililazar la palanca izquierda del mando de XBOX o de PS4 o WASD/flacheas de direccion del teclado
	public void palancaIzquierda (float x, float y){
		Vector2 mov = new Vector2 (x, y);
		transform.Translate (mov*Time.deltaTime*velocidad);
		if (mov != Vector2.zero){
			anim.SetFloat("movX", mov.x);
			anim.SetFloat("movY", mov.y);
			anim.SetBool("walking", true);
		}
	}

	//Este metodo es llamado al utililazar la palanca derecha del mando de XBOX o de PS4 o WASD/flacheas de direccion del teclado
	public void palancaDerecha (float x, float y){


		apuntador.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, ((Mathf.Atan2 (-1 * y, x)) * Mathf.Rad2Deg)));

	}

	//Este metodo es llamado al utililazar el boton A del mando de XBOX o el boton Cuadrado de PS4 o Clic izquierdo
	public void fire1 (){
		if (tiempoUltimoDisparo > tiempoEntreDisparos && !esRecarga && municionActual != 0) {
			sonidoDisparo.Play ();
			municionActual--;
			actualizarInterfaz ();
			GameObject aux = Instantiate (bala.gameObject, (cañon.transform.position), apuntador.transform.rotation);
			//aux.transform.parent = this.transform;
			tiempoUltimoDisparo = 0;
		}
	}

	//Este metodo es llamado al utililazar el boton B del mando de XBOX o el boton X de PS4 o Clic derecho
	public void fire2 (){

		if (!esRecarga) {
			tiempoDesdeRecarga = 0;
			esRecarga = true;
			recarga.gameObject.SetActive (true);
		}

	}            

	//Este metodo es llamado al utililazar el boton X del mando de XBOX o el boton Circulo de PS4 o Clic de rueda
	public void fire3 (){


	}

	//Este metodo es llamado al utililazar el boton Y del mando de XBOX o el boton Triangulo de PS4 o Space del teclado
	public void fire4 (){


	}

	//Metodo para hacer que el jugador se cague en los pantalones
	//daño: la cantidad de unidades de vida que el jugador va a perder
	public void recibirDaño (int daño){
		puntosDeVida -= daño;
		if (puntosDeVida <= 0) {
			jajajajaNoob ();
		} else {
			actualizarInterfaz ();
		}
	}


	//Metodo que se llama cuando el jugador es tan malo que no se puede pasar el juego :V
	void jajajajaNoob (){
		//Fin de la partida
		gameManager.SendMessage("restart");
		puntosDeVida = 3;
		puntosDeVidaMax = 3;
		fire2 ();
		actualizarInterfaz ();
	}

	//Este metodo se llama cuando el jugador colisione contra un trigger durante la partida
	//coll: Collider2D del objeto contra el que colisionamos
	void OnTriggerEnter2D(Collider2D coll){
		//Si el objeto es comida
		if (coll.gameObject.tag == "Food") {
			Destroy(coll.gameObject);
			puntosDeVida += 1;
			puntosDeVidaMax += 1;
			if (puntosDeVida > puntosDeVidaMax) {
				puntosDeVida = puntosDeVidaMax;
			}
			actualizarInterfaz ();
		}
		else if (coll.gameObject.tag == "ManoD") {
			equiparArma (coll.gameObject);
			Destroy(coll.gameObject);
		}

		//Si el objeto es un cofre
		else if (coll.gameObject.tag == "Cofre") {

			GameObject armaAGenerar = seleccionarRecompensa ();
			Instantiate (armaAGenerar, coll.transform.position, Quaternion.identity);

			Destroy(coll.gameObject);
		}
	}

	public void equiparArma(GameObject arma){
		SpriteRenderer imgAux = arma.GetComponent<SpriteRenderer> ();
		apuntador.gameObject.GetComponent<SpriteRenderer> ().sprite = imgAux.sprite;
		municionActual = arma.GetComponent<Item>().municionMax;
		municionTotal = arma.GetComponent<Item>().municionMax;
		tiempoEntreDisparos = arma.GetComponent<Item>().dps;
		actualizarInterfaz ();
	}
		
	public void actualizarInterfaz (){

		for (int i = 0; i < puntosDeVidaMaxTope; i++) {
			if (i < puntosDeVidaMax) {
				if (i < puntosDeVida) {
					ImagenesVidas [i].gameObject.SetActive (true);
					ImagenesVidasVacias[i].gameObject.SetActive (true);
				} else {
					ImagenesVidas [i].gameObject.SetActive (false);
					ImagenesVidasVacias[i].gameObject.SetActive (true);
				}
			} else {

				ImagenesVidas [i].gameObject.SetActive (false);
				ImagenesVidasVacias[i].gameObject.SetActive (false);
			}
		}
		for (int i = 0; i < 31; i++) {
			if (i < municionActual) {
				ImagenesBalas [i].gameObject.SetActive (true);
			} else {

			
				ImagenesBalas [i].gameObject.SetActive (false);

			}
		}
	}

	public GameObject seleccionarRecompensa(){

		int seleccion = Random.Range (0, 100);
		GameObject recompensa = null;

		if (seleccion < 30) {recompensa = seleccionarArma ();
		} else if (seleccion >= 30 && seleccion < 70) {recompensa = bebida;
		} else {recompensa = gofre;
		}

		return recompensa;

	}

	public GameObject seleccionarArma(){

		int seleccion = Random.Range (0, 100);
		int armaSeleccionada;

		if (seleccion < 20) {armaSeleccionada = 0;
		} else if (seleccion >= 20 && seleccion < 40) {armaSeleccionada = 1;
		} else if (seleccion >= 40 && seleccion < 55) {armaSeleccionada = 2;
		} else if (seleccion >= 55 && seleccion < 68) {armaSeleccionada = 3;
		} else if (seleccion >= 68 && seleccion < 78) {armaSeleccionada = 4;
		} else if (seleccion >= 78 && seleccion < 85) {armaSeleccionada = 5;
		} else if (seleccion >= 85 && seleccion < 93) {armaSeleccionada = 6;
		} else {armaSeleccionada = 7;
		}

		return armas [armaSeleccionada];

	}

	void OnCollisionEnter2D(Collision2D collider) {
		if (collider.gameObject.tag.Equals ("BulletEnemy")) {
			recibirDaño (1);
			Destroy (collider.gameObject);
		}
	}

}