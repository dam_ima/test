﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerSoloMenuInicioEsQueSiNoPetaPirLaInterfaz : MonoBehaviour {


	//Variable
	private Animator anim;
	public float velocidad = 3;
	protected Joystick joystick;
	public GameObject[] armas;
	private GameObject armaSeleccionada;
	private GameObject yo;
	//Se ejecuta al principio

	void Awake (){
		//SceneManager.sceneLoaded += OnSceneLoaded;
		joystick = FindObjectOfType<Joystick> ();
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor) {
			GameObject mobileButton = GameObject.Find ("Fixed Joystick");
			joystick = FindObjectOfType<Joystick> ();
		} else {
			GameObject mobileButton = GameObject.Find ("Fixed Joystick");
			mobileButton.SetActive (false);
		}
		if (!yo) {
			yo = this.gameObject;
		} else {
			Destroy (this.gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}

	/*void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		if (scene.name.Equals ("EscenaPrincipal")) {
			//Player player = GameObject.Find ("PlayerFInalFinalV3(Clone)").GetComponent<Player>();
			if(this.gameObject) {
				GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager2>().armaInicial = armaSeleccionada;
				Destroy (this.gameObject);
			}
		}
	}*/

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Cofre") {

			GameObject armaAGenerar = seleccionarRecompensa ();
			Instantiate (armaAGenerar, coll.transform.position, Quaternion.identity);

			Destroy(coll.gameObject);
		}

		if (coll.gameObject.tag == "ManoD") {
			Destroy(coll.gameObject);
			armaSeleccionada = coll.gameObject;
		}
	}

	public GameObject seleccionarRecompensa(){

		GameObject recompensa = null;
		recompensa = seleccionarArma ();
		return recompensa;

	}

	public GameObject seleccionarArma(){

		int seleccion = Random.Range (0, 100);
		int armaSeleccionada;

		if (seleccion < 20) {armaSeleccionada = 0;
		} else if (seleccion >= 20 && seleccion < 40) {armaSeleccionada = 1;
		} else if (seleccion >= 40 && seleccion < 55) {armaSeleccionada = 2;
		} else if (seleccion >= 55 && seleccion < 68) {armaSeleccionada = 3;
		} else if (seleccion >= 68 && seleccion < 78) {armaSeleccionada = 4;
		} else if (seleccion >= 78 && seleccion < 85) {armaSeleccionada = 5;
		} else if (seleccion >= 85 && seleccion < 93) {armaSeleccionada = 6;
		} else {armaSeleccionada = 7;
		}

		return armas [armaSeleccionada];

	}

	// Update is called once per frame
	void Update () {
		

		if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) {
			palancaIzquierda (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
		}else {
			anim.SetBool("walking", false);
		}

		if (Application.platform == RuntimePlatform.Android) {
			palancaIzquierda (joystick.Horizontal, joystick.Vertical);
		}
	}

	//Este metodo es llamado al utililazar la palanca izquierda del mando de XBOX o de PS4 o WASD/flacheas de direccion del teclado
	public void palancaIzquierda (float x, float y){
		Vector2 mov = new Vector2 (x, y);
		transform.Translate (mov*Time.deltaTime*velocidad);
		if (mov != Vector2.zero){
			anim.SetFloat("movX", mov.x);
			anim.SetFloat("movY", mov.y);
			anim.SetBool("walking", true);
		}
	}

}