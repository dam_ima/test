﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager2 : MonoBehaviour {

	private static BoardManager2 boardScript = null;
	public static GameManager2 instance = null;
	public int level = 0;
	private Text textoNivel;
	private GameObject player;
	private bool isReset = false;
	public GameObject reward;
	public GameObject armaInicial;
	// Use this for initialization
	void Awake () {

		SceneManager.sceneLoaded += OnSceneLoaded;
		if(instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);

		}

		if(boardScript == null) {
			boardScript = gameObject.GetComponent<BoardManager2> ();
		} 
		DontDestroyOnLoad(gameObject);

		textoNivel = GameObject.Find ("levelCount").GetComponent<Text>();
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnEnable()
	{ //Tell our ‘OnLevelFinishedLoading’ function to start listening for a scene change event as soon as this script is enabled. 
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable()
	{ //Tell our ‘OnLevelFinishedLoading’ function to stop listening for a scene change event as soon as this script is disabled. //Remember to always have an unsubscription for every delegate you subscribe to! 
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode){

		if(scene.name.Equals("MenuInicio")){
			player = GameObject.Find ("PlayerFInalFinalV3(Clone)");
			Destroy (GameObject.Find ("CanvasFinal"));
			GameObject bola = GameObject.FindGameObjectWithTag ("BoardManager");
			Destroy (GameObject.Find ("Board"));
			Destroy (player);
			Destroy (this.gameObject);
			GameObject rewardSpawn = GameObject.FindGameObjectWithTag ("reward");
			/*GameObject instance =
			Instantiate (reward, new Vector2 (rewardSpawn.transform.position.x, rewardSpawn.transform.position.y), Quaternion.identity) as GameObject;
			*/
			isReset = true;
		}
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
		if (!isReset) {
			initGame ();
			textoNivel.text = "Nivel: " + level;
			level++;
		} else {
			isReset = false;
		}
	}

	void initGame() {
		boardScript.setupLevel (level);
	}

	void restart(){
		level = 0;
		SceneManager.LoadScene ("MenuInicio",LoadSceneMode.Single);
	}

	void nextLevel() {
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}
		
	// Update is called once per frame
	void Update () {
		
	}
}
